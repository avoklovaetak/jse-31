package ru.volkova.tm.component;

import org.jetbrains.annotations.NotNull;
import ru.volkova.tm.bootstrap.Bootstrap;
import ru.volkova.tm.command.data.BackupLoadJsonCommand;
import ru.volkova.tm.command.data.BackupSaveJsonCommand;
import ru.volkova.tm.entity.Command;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Backup implements Runnable {

    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    private final Bootstrap bootstrap;

    private static final int INTERVAL = 30;

    private static final String NAME_LOAD = new BackupLoadJsonCommand().name();

    private static final String NAME_SAVE = new BackupSaveJsonCommand().name();

    public Backup(@NotNull Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void init() {
        load();
        start();
    }

    public void start() {
        es.scheduleWithFixedDelay(this, 0, INTERVAL, TimeUnit.SECONDS);
    }

    public void stop() {
        es.shutdown();
    }

    public void load() {
        bootstrap.parseCommand(NAME_LOAD);
    }

    public void run(){
        bootstrap.parseCommand(NAME_SAVE);
    }

}
