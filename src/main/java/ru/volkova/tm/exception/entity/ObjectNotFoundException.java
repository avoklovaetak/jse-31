package ru.volkova.tm.exception.entity;

import ru.volkova.tm.exception.AbstractException;

public class ObjectNotFoundException extends AbstractException {

    public ObjectNotFoundException() {
        super("Error! Object not found... ");
    }

}
