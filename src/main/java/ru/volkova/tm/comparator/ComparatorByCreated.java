package ru.volkova.tm.comparator;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.volkova.tm.api.entity.IHasCreated;

import java.util.Comparator;

@NoArgsConstructor
public final class ComparatorByCreated implements Comparator<IHasCreated> {

    @NotNull
    private final static ComparatorByCreated INSTANCE = new ComparatorByCreated();

    @NotNull
    public static ComparatorByCreated getInstance() {
        return INSTANCE;
    }

    @Override
    public int compare(final IHasCreated o1, final IHasCreated o2) {
        if (o1 == null || o2 == null) return 0;
        return o1.getCreated().compareTo(o2.getCreated());
    }

}
