package ru.volkova.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.volkova.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandRepository {

    void add(@NotNull AbstractCommand command);

    @NotNull
    Collection<AbstractCommand> getArguments();

    @NotNull
    Collection<AbstractCommand> getArgsCommandsList();

    @NotNull
    AbstractCommand getCommandByArg(@NotNull String arg);

    @NotNull
    AbstractCommand getCommandByName(@NotNull String name);

    @NotNull
    Collection<AbstractCommand> getCommands();

}
