package ru.volkova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.repository.IOwnerRepository;
import ru.volkova.tm.entity.AbstractOwnerEntity;
import ru.volkova.tm.enumerated.Status;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public interface IOwnerService<E extends AbstractOwnerEntity> extends IOwnerRepository<E> {

    @NotNull
    Optional<E> changeOneStatusById(
            @NotNull String userId,
            @Nullable String id,
            @Nullable Status status
    );

    @NotNull
    Optional<E> changeOneStatusByIndex(
            @NotNull String userId, Integer index, Status status);

    @NotNull
    Optional<E> changeOneStatusByName(
            @NotNull String userId,
            @Nullable String name,
            @Nullable Status status
    );

    @NotNull
    List<E> findAll(@NotNull String userId, @Nullable Comparator<E> comparator);

    @NotNull
    Optional<E> findOneByIndex(@NotNull String userId, @Nullable Integer index);

    @NotNull
    Optional<E> findOneByName(@NotNull String userId, @Nullable String name);

    @NotNull
    Optional<E> finishOneById(@NotNull String userId, @Nullable String id);

    @NotNull
    Optional<E> finishOneByIndex(@NotNull String userId, @Nullable Integer index);

    @NotNull
    Optional<E> finishOneByName(@NotNull String userId, @Nullable String name);

    void removeOneByIndex(@NotNull String userId, @Nullable Integer index);

    void removeOneByName(@NotNull String userId, @Nullable String name);

    @NotNull
    Optional<E> startOneById(@NotNull String userId, @Nullable String id);

    @NotNull
    Optional<E> startOneByIndex(@NotNull String userId, @Nullable Integer index);

    @NotNull
    Optional<E> startOneByName(@NotNull String userId, @Nullable String name);

    @NotNull
    Optional<E> updateOneById(
            @NotNull String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description);

    @NotNull
    Optional<E> updateOneByIndex(
            @NotNull String userId,
            @Nullable Integer index,
            @Nullable String name,
            @Nullable String description
    );

}
