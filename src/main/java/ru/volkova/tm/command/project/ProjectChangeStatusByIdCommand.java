package ru.volkova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.enumerated.Role;
import ru.volkova.tm.enumerated.Status;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;
import ru.volkova.tm.exception.entity.ProjectNotFoundException;
import ru.volkova.tm.entity.Project;
import ru.volkova.tm.util.TerminalUtil;

import java.util.Arrays;

public class ProjectChangeStatusByIdCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "change project status by id";
    }

    @Override
    public void execute() {
        if (serviceLocator == null) throw new ObjectNotFoundException();
        System.out.println("[CHANGE PROJECT STATUS]");
        System.out.println("ENTER ID");
        @Nullable final String userId = serviceLocator.getAuthService().getUserId();
        @Nullable final String id = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS");
        System.out.println(Arrays.toString(Status.values()));
        @Nullable final String statusId = TerminalUtil.nextLine();
        @Nullable final Status status = Status.valueOf(statusId);
        serviceLocator.getProjectService()
                .changeOneStatusById(userId, id, status);
    }

    @NotNull
    @Override
    public String name() {
        return "project-change-status-by-id";
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
