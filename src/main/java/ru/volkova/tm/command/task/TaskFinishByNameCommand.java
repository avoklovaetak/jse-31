package ru.volkova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.enumerated.Role;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;
import ru.volkova.tm.exception.entity.TaskNotFoundException;
import ru.volkova.tm.entity.Task;
import ru.volkova.tm.util.TerminalUtil;

import java.util.Optional;

public class TaskFinishByNameCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "finish task by name";
    }

    @Override
    public void execute() {
        if (serviceLocator == null) throw new ObjectNotFoundException();
        System.out.println("[FINISH TASK]");
        System.out.println("ENTER NAME:");
        @Nullable final String name = TerminalUtil.nextLine();
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        @NotNull final Optional<Task> task = serviceLocator.getTaskService().finishOneByName(userId, name);
        if (!task.isPresent()) throw new TaskNotFoundException();
    }

    @NotNull
    @Override
    public String name() {
        return "task-finish-by-name";
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
