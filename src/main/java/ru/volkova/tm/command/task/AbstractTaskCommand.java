package ru.volkova.tm.command.task;

import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.command.AbstractCommand;
import ru.volkova.tm.exception.entity.TaskNotFoundException;
import ru.volkova.tm.entity.Task;

public abstract class AbstractTaskCommand extends AbstractCommand {

    protected void showTask(@Nullable final Task task) {
        if (task == null) throw new TaskNotFoundException();
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + task.getStatus().getDisplayName());
    }

}
